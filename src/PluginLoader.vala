namespace Replay {
	public class PluginLoader : GLib.Object {
		Gee.ArrayList<Vod.Plugin> info_list;
		
		construct {
			this.info_list = new Gee.ArrayList<Vod.Plugin>();
		}
		
		public void load() {
			var dir = File.new_for_path (Config.libdir() + "/vod-1.0");
			if (dir.query_exists() && dir.query_file_type (FileQueryInfoFlags.NONE) != FileType.DIRECTORY)
				dir.delete();
			if (!dir.query_exists())
				dir.make_directory_with_parents();
			var e = dir.enumerate_children ("standard::*", FileQueryInfoFlags.NOFOLLOW_SYMLINKS);
			FileInfo info = null;
			while ((info = e.next_file()) != null) {
				var child = e.get_child (info);
				var plugin = Vod.Plugin.open (child.get_path());
				if (plugin != null)
					this.info_list.add (plugin);
			}
		}
		
		public Gee.List<Type> list_types() {
			var tl = new Gee.ArrayList<Type>();
			for (var i = 0; i < this.info_list.size; i++)
				tl.add_all (this.info_list[i].types);
			return tl;
		}
		
		public Gee.List<Type> list_types_parent (Type parent) {
			var tl = new Gee.ArrayList<Type>();
			for (var i = 0; i < this.info_list.size; i++)
				this.info_list[i].types.foreach (t => {
					if (t.is_a (parent))
						tl.add (t);
					return true;
				});
			return tl;
		}
	}
}
