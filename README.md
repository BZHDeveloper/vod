# Replay
service de rattrapage TV

pré-requis :
- [meson](https://github.com/mesonbuild/meson)
- [ninja](https://github.com/ninja-build/ninja)
- [GJson](https://github.com/BZHDeveloper/vul)
- [Soup](https://git.gnome.org/browse/libsoup/)
- [Gtk](https://git.gnome.org/browse/gtk+/)

construction :
```
cd vod
mkdir build && cd build
meson ..
ninja
sudo ninja install
```

une série de plugins est disponible à cette adresse : [vod-plugins](https://gitlab.gnome.org/BZHDeveloper/vod-plugins)
