namespace Vod {
	public abstract class Channel : Gee.Hashable<Vod.Channel>, GLib.Object {
		Gee.ArrayList<Show> show_list;
		
		construct {
			this.show_list = new Gee.ArrayList<Show>();
		}
		
		public bool equal_to (Vod.Channel channel) {
			return str_equal (this.name, channel.name);
		}
		
		public uint hash() {
			return str_hash (this.name);
		}
		
		public virtual bool get_live (out Video live_video) {
			return false;
		}
		
		public Gee.List<Show> filter (Gee.Predicate<Show> func) {
			var list = new Gee.ArrayList<Show>();
			list.add_all_iterator (this.shows.filter (func));
			return list;
		}
		
		public abstract void fill (Gee.List<Show> shows) throws GLib.Error;
		
		public abstract string name { owned get; }
		
		public virtual GLib.Icon logo {
			owned get {
				return new BytesIcon (resources_lookup_data ("/resources/logos/%s.png".printf (name.down()), ResourceLookupFlags.NONE));
			}
		}
		
		public Gee.List<Show> shows {
			get {
				try {
					if (this.show_list.size == 0)
						fill (this.show_list);
				}
				catch {
				}
				return this.show_list;
			}
		}
	}
}
