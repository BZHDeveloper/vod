namespace Vod {
	[CCode (has_target = false)]
	public delegate bool PluginInitFunc (Plugin plugin);
	
	public class Plugin : GLib.Object {
		Module module;
		Gee.ArrayList<Type> type_list;
		
		internal Plugin (string path) {
			GLib.Object (path : path);
		}
		
		construct {
			this.type_list = new Gee.ArrayList<Type>();
		}
		
		public string path { get; construct; }
		
		public Gee.List<Type> types {
			get {
				return this.type_list;
			}
		}
		
		public bool load() {
			this.type_list.clear();
			this.module = Module.open (path, ModuleFlags.BIND_LAZY);
			if (this.module == null)
				return false;
			void* data;
			if (!this.module.symbol ("plugin_init", out data))
				return false;
			return ((PluginInitFunc)data)(this);
		}
		
		public void unload() {
			this.type_list.clear();
			this.module = null;
		}
		
		public static Plugin open (string path) {
			var plugin = new Plugin (path);
			if (!plugin.load())
				return null;
			return plugin;
		}
	}
}
