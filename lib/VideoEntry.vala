namespace Vod {
	public class VideoEntry : GLib.Object {
		public virtual GLib.InputStream get_stream() throws GLib.Error {
			return File.new_for_uri (this.url).read();
		}
	
		public virtual int64 size {
			get {
				var file = File.new_for_uri (this.url);
				var info = file.query_info ("standard::*", FileQueryInfoFlags.NONE);
				return info.get_size();
			}
		}
		
		public string url { get; set; }
		
		public string quality { get; set; }
		
		public string mime_type { get; set; }
	}
	
	public class M3u8VideoEntry : VideoEntry {
		public M3u8VideoEntry (M3u8Entry entry, bool live = false) {
			GLib.Object (entry : entry, live : live);
		}
		
		public override GLib.InputStream get_stream() throws GLib.Error {
			return this.entry.input_stream;
		}
			
		construct {
			this.url = entry.uri;
			this.mime_type = "video/ts";
			if (entry.infos["RESOLUTION"] != null)
				this.quality = entry.infos["RESOLUTION"];
			else if (entry.infos["BANDWIDTH"] != null)
				this.quality = entry.infos["BANDWIDTH"];
		}
		
		public override int64 size {
			get {
				return this.entry.size;
			}
		}
		
		public M3u8Entry entry { get; construct; }
		
		public bool live {
			get {
				return this.entry.input_stream.live;
			}
			set construct {
				this.entry.input_stream.live = value;
			}
		}
	}
}
