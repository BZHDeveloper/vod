namespace Vod {
	public class YoutubeDlEntry : VideoEntry {
		int64 filesize;
		
		internal YoutubeDlEntry (string uri, GJson.Node node) {
			this.url = uri;
			this.quality = node["format_id"].as_string();
			this.mime_type = "video/" + node["ext"].as_string();
			this.filesize = base.size;
			if (node["filesize"].node_type == GJson.NodeType.INTEGER)
				this.filesize = node["filesize"].as_integer();
		}
		
		public override int64 size {
			get {
				return this.filesize;
			}
		}
		
		public override GLib.InputStream get_stream() throws GLib.Error {
			var proc = new Subprocess (SubprocessFlags.STDERR_SILENCE | SubprocessFlags.STDIN_PIPE | SubprocessFlags.STDOUT_PIPE,
				"youtube-dl",
				this.url,
				"-f",
				this.quality,
				"-o",
				"-");
			return proc.get_stdout_pipe();
		}
	}
	
	public class YoutubeDl : Video {
		Gee.ArrayList<VideoEntry> entry_list;
		
		internal YoutubeDl (string uri, GJson.Object object) {
			this.entry_list = new Gee.ArrayList<VideoEntry>();
			this.description = object["description"].as_string();
			this.title = object["title"].as_string();
			string date = object["upload_date"].as_string();
			int y = int.parse (date.substring (0, 4));
			int m = int.parse (date.substring (4, 2));
			int d = int.parse (date.substring (6, 2));
			this.release_date = new DateTime.local (y, m, d, 0, 0, 0);
			object["formats"].foreach (format => {
				this.entry_list.add (new YoutubeDlEntry (uri, format));
				return true;
			});
		}
		
		public override void fill (Gee.List<VideoEntry> entries) throws GLib.Error {
			entries.add_all (this.entry_list);
		}
		
		public static YoutubeDl open (string uri) throws GLib.Error {
			string output = "", err = "";
			Process.spawn_command_line_sync ("youtube-dl %s -j --no-warnings".printf (Uri.escape_string (uri, Uri.RESERVED_CHARS_ALLOWED_IN_PATH)), 
				out output, 
				out err);
			if (err.strip().length > 0)
				throw new IOError.FAILED (err.strip());
			return new YoutubeDl (uri, GJson.Object.parse (output.strip()));
		}
	}
}
