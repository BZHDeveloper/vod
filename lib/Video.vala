namespace Vod {
	public static bool try_parse_date (string str, out DateTime date) {
		date = new DateTime.now_local();
		TimeVal tv = TimeVal();
		if (!tv.from_iso8601 (str))
			return false;
		date = new DateTime.from_timeval_local (tv);
		return true;
	}
	
	public static DateTime parse_date (string str) {
		DateTime result = null;
		try_parse_date (str, out result);
		return result;
	}
	
	public abstract class Video : Gee.Hashable<Vod.Video>, GLib.Object {
		Gee.ArrayList<VideoEntry> list;
		
		construct {
			this.list = new Gee.ArrayList<VideoEntry>();
		}
		
		public virtual bool equal_to (Vod.Video video) {
			return str_equal (this.title, video.title);
		}
		
		public virtual uint hash() {
			return str_hash (this.title);
		}
		
		public abstract void fill (Gee.List<VideoEntry> entries) throws GLib.Error;
		
		public string title { get; set; }
		
		public string description { get; set; }
		
		public DateTime release_date { get; set; }
		
		public GLib.Icon thumbnail { get; set; }
		
		public Gee.List<VideoEntry> entries {
			get {
				try {
					if (this.list.size == 0)
						fill (this.list);
				}
				catch {}
				return this.list;
			}
		}
	}
}
