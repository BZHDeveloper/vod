namespace Vod {
	public abstract class Show : Gee.Hashable<Show>, GLib.Object {
		Gee.ArrayList<Video> video_list;
		
		construct {
			this.video_list = new Gee.ArrayList<Video>();
		}
		
		public virtual bool equal_to (Vod.Show show) {
			return str_equal (this.name, show.name);
		}
		
		public virtual uint hash() {
			return str_hash (this.name);
		}
		
		public abstract void fill (Gee.List<Video> videos) throws GLib.Error;
		
		public string name { get; set; }
		
		public string description { get; set; }
		
		public Gee.List<Video> videos {
			get {
				try {
					if (this.video_list.size == 0)
						fill (this.video_list);
				}
				catch {
				}
				return this.video_list;
			}
		}
	}
}
