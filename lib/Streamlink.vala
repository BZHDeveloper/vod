namespace Vod {
	public class StreamlinkEntry : Vod.VideoEntry {
		internal StreamlinkEntry (string url, string quality) {
			this.url = url;
			this.quality = quality;
			this.mime_type = "application/streamlink";
		}
		
		public override GLib.InputStream get_stream() throws GLib.Error {
			var proc = new Subprocess (SubprocessFlags.STDERR_SILENCE | SubprocessFlags.STDIN_PIPE | SubprocessFlags.STDOUT_PIPE,
				"streamlink",
				this.url,
				this.quality,
				"--stdout");
			return proc.get_stdout_pipe();
		}
	}
	
	public class Streamlink : Vod.Video {
		Gee.ArrayList<VideoEntry> entry_list;
		
		internal Streamlink (string uri, GJson.Object object) {
			this.entry_list = new Gee.ArrayList<VideoEntry>();
			object.foreach (prop => {
				entry_list.add (new StreamlinkEntry (uri, prop.name));
				return true;
			});
		}
		
		public override void fill (Gee.List<VideoEntry> entries) throws GLib.Error {
			entries.add_all (entry_list);
		}
		
		public static Streamlink open (string uri) throws GLib.Error {
			string output = "", err = "";
			Process.spawn_command_line_sync ("streamlink %s --json".printf (Uri.escape_string (uri, Uri.RESERVED_CHARS_ALLOWED_IN_PATH)),
				out output,
				out err);
			if (err.strip().length > 0)
				throw new IOError.FAILED (err.strip());
			var object = GJson.Object.parse (output.strip());
			if (object["error"].node_type == GJson.NodeType.STRING)
				throw new IOError.INVALID_DATA (object["error"].as_string());
			return new Streamlink (uri, object["streams"].as_object());
		}
	}
}
