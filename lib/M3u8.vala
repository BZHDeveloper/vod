namespace Vod {
	public class M3u8InputStream : GLib.InputStream {
		M3u8Entry entry;
		Gee.ArrayQueue<uint8> buffer;
		int index;
		Soup.Session session;
		
		internal M3u8InputStream (M3u8Entry entry) {
			this.index = 0;
			this.entry = entry;
			this.buffer = new Gee.ArrayQueue<uint8>();
			this.session = new Soup.Session();
			this.session.user_agent = "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.114 Mobile Safari/537.36";
		}
		
		void feed() {
			if (this.index + 1 == this.entry.files.size) {
				if (!this.live)
					return;
				this.index = 0;
				this.entry.load();
			}
			var message = new Soup.Message ("GET", this.entry.files[this.index].uri);
			this.session.send_message (message);
			this.buffer.add_all_array (message.response_body.data);
			this.index++;
		}
		
		public bool live { get; set; }
		
		public override bool close (Cancellable? cancellable = null) throws IOError {
			this.buffer.clear();
			this.index = 0;
			return true;
		}
		
		public override ssize_t read (uint8[] data, Cancellable? cancellable = null) throws IOError {
			print ("euuuh...\n");
			var count = 0;
			for (var i = 0; i < data.length; i++) {
				if (buffer.size == 0)
					feed();
				if (buffer.size == 0)
					return count;
				data[i] = buffer.poll_head();
				count++;
			}
			return count;
		}
	}

	public class M3u8File : GLib.Object {
		internal M3u8File (double duration, string uri, string base_uri, string parameters) {
			this.duration = duration;
			if (!uri.has_prefix ("http://") && !uri.has_prefix ("https://"))
				this.uri = "%s/%s".printf (base_uri, uri);
			else
				this.uri = uri;
			if (parameters.length > 0 && !uri.contains ("?"))
				this.uri = this.uri + parameters;
		}
		
		public double duration { get; private set; }
		
		public string uri { get; private set; }
	}

	public class M3u8Entry : GLib.Object {
		Gee.HashMap<string, string> info_map;
		string parameters;
		Soup.Session session;
		Gee.ArrayList<M3u8File> file_list;
		string base_uri;
		
		internal M3u8Entry (string line, string uri, string base_uri, string? prms = null) {
			this.base_uri = base_uri;
			this.file_list = new Gee.ArrayList<M3u8File>();
			this.info_map = new Gee.HashMap<string, string>();
			this.session = new Soup.Session();
			
			if (!uri.has_prefix ("http://") && !uri.has_prefix ("https://"))
				this.uri = "%s/%s".printf (base_uri, uri);
			else
				this.uri = uri;
			string s = this.uri.split (base_uri)[1];
			if (s.contains ("?"))
				this.parameters = s.substring (s.index_of ("?"));
			else
				this.parameters = "";
			if (this.parameters == "" && prms != null) {
				this.parameters = "?" + prms.dup();
				this.uri += this.parameters;
			}
			var reader = new GText.StringReader (line);
			while (!reader.eof) {
				var key_builder = new StringBuilder();
				var value_builder = new StringBuilder();
				unichar u = 0;
				while (reader.peek().isspace())
					reader.read();
				while ((u = reader.read()) != '=')
					key_builder.append_unichar (u);
				if (reader.peek() == '"') {
					reader.read();
					while (reader.peek() != '"' && !reader.eof) {
						u = reader.read();
						if (u == '\\') {
							value_builder.append_unichar ('\\');
							value_builder.append_unichar (reader.read());
						}
						else
							value_builder.append_unichar (u);
					}
					this.info_map[key_builder.str] = value_builder.str;
					if (reader.peek() == '"')
						reader.read();
					if (reader.peek() == ',')
						reader.read();
				}
				else if (reader.eof || reader.peek() == ',') {
					this.info_map[key_builder.str] = null;
					if (reader.peek() == ',')
						reader.read();
				}
				else {
					while (reader.peek() != ',' && !reader.eof) {
						u = reader.read();
						value_builder.append_unichar (u);
					}
					this.info_map[key_builder.str] = value_builder.str;
					if (reader.peek() == ',')
						reader.read();
				}
			}
		
			this.session.user_agent = "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.114 Mobile Safari/537.36";
			
			load();
		}
		
		public void load() {
			this.file_list.clear();
			var message = new Soup.Message ("GET", this.uri);
			session.send_message (message);
			FileUtils.set_data ("/home/yannick/Documents/data1", message.response_body.data);
			var dis = new DataInputStream (this.session.send (message));
			string l = null;
			while ((l = dis.read_line()) != null) {
				if (l.has_prefix ("#EXT-X-TARGETDURATION:"))
					this.target_duration = double.parse (l.split ("#EXT-X-TARGETDURATION:")[1].strip());
				if (l.has_prefix ("#EXTINF:")) {
					uint duration = (uint)int.parse (l.split ("#EXTINF:")[1].split (",")[0].strip());
					l = dis.read_line().strip();
					while (l[0] == '#')
						l = dis.read_line().strip();
					this.file_list.add (new M3u8File (duration, l, base_uri, this.parameters));
				}
			}
			this.input_stream = new M3u8InputStream (this);
			
			message = new Soup.Message ("GET", this.file_list[0].uri);
			this.session.send_message (message);
			int len = message.response_body.data.length;
			double fdur = this.file_list[0].duration;
			double dur = 0;
			foreach (var entry in this.file_list)
				dur += entry.duration;
			if (fdur == 0)
				this.size = this.file_list.size * len;
			else
				this.size = (int64)(len * dur / fdur);
		}
		
		public int64 size { get; private set; }
		
		public M3u8InputStream input_stream { get; private set; }
		
		public double target_duration { get; private set; }
		
		public string uri { get; private set; }
		
		public Gee.Map<string, string> infos {
			get {
				return info_map;
			}
		}
		
		public Gee.List<M3u8File> files {
			get {
				return this.file_list;
			}
		}
	}

	public class M3u8 : GLib.Object {
		public M3u8 (string uri) {
			GLib.Object (uri : uri);
		}
		
		Soup.Session session;
		Gee.ArrayList<M3u8Entry> entry_list;
		string base_uri;
		
		construct {
			this.entry_list = new Gee.ArrayList<M3u8Entry>();
			this.session = new Soup.Session();
			this.session.user_agent = "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.114 Mobile Safari/537.36";
			var message = new Soup.Message ("GET", this.uri);
			session.send_message (message);
			FileUtils.set_data ("/home/yannick/Documents/data", message.response_body.data);
			var dis = new DataInputStream (this.session.send (message));
			this.real_uri = message.uri.to_string (false);
			string prms = null;
			if (message.uri.query != null)
				prms = message.uri.query;
			this.base_uri = File.new_for_uri (this.real_uri).get_parent().get_uri();
			string line = null;
			while ((line = dis.read_line()) != null) {
				if (line.has_prefix ("#EXT-X-VERSION:"))
					this.version = (uint)int.parse (line.split ("#EXT-X-VERSION:")[1].strip());
				if (line.has_prefix ("#EXT-X-STREAM-INF:")) {
					this.entry_list.add (new M3u8Entry (line.substring ("#EXT-X-STREAM-INF:".length).strip(), dis.read_line().strip(), this.base_uri, prms));
				}
			}
			if (this.entry_list.size == 0)
				this.entry_list.add (new M3u8Entry ("RESOLUTION=STANDARD", this.uri, this.base_uri, prms));
		}
		
		public Gee.List<M3u8Entry> entries {
			get {
				return entry_list;
			}
		}
		
		public uint version { get; private set; }
		
		public string real_uri { get; private set; }
		
		public string uri { get; construct; }
	}
}
